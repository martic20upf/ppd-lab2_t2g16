/* PI */ 

#include <stdio.h>
#include <mpi.h>
#include <math.h>

#define PI_REF 3.141592653589793238462643

/* Function f(x) */
double f(double a)
{
    return (4.0 / (1.0 + a*a));
}

/* Main program */
int main (int argc, char ** argv)
{
  int rank, num, n_rectangles;
  double r_width, r_height, area_sum, x;

  /* MPI Initialization */
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &num);

  if(argc == 2){
    sscanf(argv[1], "%d", &n_rectangles);
  }else{
    printf("ERROR: One integer argument needed.");
  }

  r_width = 1.0/n_rectangles; 
  x = 0 + r_width*rank;
  while(x<1){
    r_height = (f(x) + f(x + r_width))/2;
    area_sum += r_height*r_width;
    x += r_width*num;
  }

  float area_result, area_recv;
  area_result = (float) area_sum;

  MPI_Reduce(&area_result,&area_recv, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

  if (rank == 0){
    printf("rect. = %d\n",n_rectangles);
    printf("Pi = %f\n", area_recv);
    printf("Error = %f\n", fabs(PI_REF - area_recv));
    fflush (stdout);
  }

  /* End MPI */
  MPI_Finalize ();
  return 0;
}

