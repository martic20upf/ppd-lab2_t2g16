/* Hello World! */

#include <string.h>
#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>

int main (int argc, char ** argv)
{
  int rank;
  char* to_print = (char*) malloc(100);

  MPI_Init(&argc,&argv); 

  //get rank
  MPI_Comm_rank(MPI_COMM_WORLD,&rank); 
  if(rank==0){

    //get mpi info
    char* info = (char *) malloc(150);
    int res_len;
    MPI_Get_library_version(info, &res_len);

    //get n_procs
    int n_procs;
    MPI_Comm_size(MPI_COMM_WORLD,&n_procs); 
    
    //get version
    char text[] = "Open MPI v";
    char* p_s = strstr( info, text) + sizeof(text) - 1;
    char* p_e = strstr( info, ", package:");
    long int len = (p_e - p_s)/sizeof(char);
    char* version = (char*) malloc(len);
    memcpy(version, p_s, len);

    //get ip
    char text2[] = "@";
    p_s = strstr(info, text2) + sizeof(text2) - 1;
    p_e = strstr(info, " Distribution,");
    len = (p_e - p_s)/sizeof(char);
    char* ip = (char*) malloc(len);
    memcpy(ip, p_s, len);

    //send the message
    char* buffer = (char*) malloc(100);
    for(int i = 1;i<n_procs;i++){
      sprintf(buffer, "Hello world! from %s", ip);
      MPI_Send(buffer, 100, MPI_CHAR, i, 0, MPI_COMM_WORLD);
    }
    free(buffer);

    //Parent msg to print
    sprintf(to_print, "MPI Version: %s", version);

    //free memory
    free(version);
    free(ip);
  }else{
    MPI_Status stat;
    char* buffer = (char*) malloc(100);
    
    //recieve message
    MPI_Recv(buffer, 100, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &stat);
    sprintf(to_print, "%s", buffer);
    free(buffer);
  } 

  //print text
  printf("Process %d : %s\n", rank, to_print);
  free(to_print);

  MPI_Finalize(); 
  return 0; 
}

