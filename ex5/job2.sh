#!/bin/bash

#SBATCH --job-name=ex5
#SBATCH --workdir=.
#SBATCH --output=covid_%j.out
#SBATCH --error=covid_%j.err
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=12
#SBATCH --nodes=1
#SBATCH --time=00:00:30

source /shared/profiles.d/easybuild.sh
module load OpenMPI/3.1.4-GCC-8.3.0

make || exit 1      # Exit if make fails
mpirun ./covid
