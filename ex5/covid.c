/* COVID-19 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

/* Definitions */
#define NUM_CONT 6
#define FILENAME "spread.dat"

typedef struct {
     int id;
     int continent;
     float Re;
  }  tZone;

/* Main program */
int main (int argc, char **argv)
{
   int id, n, id_size, zones_x_id, module, seek, stop_seek;
   MPI_File file;
   MPI_Offset offset, file_size;

   MPI_Init(NULL,NULL);
   MPI_Comm_rank(MPI_COMM_WORLD, &id);
   MPI_Comm_size (MPI_COMM_WORLD, &id_size);

   MPI_Datatype zone_type, oldtypes[2];
   int blockcounts[2];
   MPI_Aint offsets[2], lb, extent;

   /*description of 2 first int fields id and continent*/
   offsets[0] = 0;
   oldtypes[0] = MPI_INT;
   blockcounts[0] = 2;

   /*description of las float fields Re*/
   MPI_Type_get_extent(MPI_INT, &lb, &extent);
   offsets[1] = 2 * extent;
   oldtypes[1] = MPI_FLOAT;
   blockcounts[1] = 1;

   /* define structured type and commit it */
   MPI_Type_create_struct (2, blockcounts, offsets, oldtypes, &zone_type);
   MPI_Type_commit (&zone_type);

   MPI_File_open (MPI_COMM_WORLD, FILENAME, MPI_MODE_RDONLY, MPI_INFO_NULL, &file);

   // divide work
   MPI_File_get_size(file, &file_size);
   n=file_size/sizeof(tZone);
   zones_x_id = n/id_size;
   module = n%id_size;
   int i = 0;
   while(i<id){
      seek += zones_x_id;
      if(i<module) seek++;
      i++;
   }
   seek=seek*sizeof(tZone);
   //calculate stop seek
   stop_seek = seek+zones_x_id*sizeof(tZone);
   if(id<module) stop_seek+=sizeof(tZone);
   float max[NUM_CONT];
   for(int j=0;j<NUM_CONT;j++)max[j]=0;
   
   while(seek<stop_seek){
      tZone zone_temp;
      MPI_Status stat;
      int count, rc1, rc2;
      // rc1 = MPI_File_read_all(file, &zone_temp, 1, zone_type, &stat);
      MPI_File_read_at(file, seek, &zone_temp, 1, zone_type, &stat);      
      if(zone_temp.Re>max[zone_temp.continent]){
         max[zone_temp.continent]=zone_temp.Re;
      }
      seek+=sizeof(tZone);
   }
   
   if(id<module) zones_x_id++;
   printf("Proc %*d (%d):     %.2f     %.2f     %.2f     %.2f     %.2f     %.2f\n", 
      2, id, zones_x_id, max[0], max[1], max[2], max[3], max[4], max[5]);
   

   float max_recv[NUM_CONT];
   MPI_Barrier(MPI_COMM_WORLD);
   MPI_Reduce(max, max_recv, NUM_CONT, MPI_FLOAT, MPI_MAX, 0, MPI_COMM_WORLD);
   if(id==0){
      printf("---------------------------------------------------------------------\n");
      printf("Final Max (%*d):%.2f     %.2f     %.2f     %.2f     %.2f     %.2f\n", 
         7, n, max_recv[0], max_recv[1], max_recv[2], max_recv[3], max_recv[4], max_recv[5]);
      fflush (stdout);
   }

   MPI_File_close(&file);
   MPI_Type_free(&zone_type);
   MPI_Finalize ();
   return 0;
}

